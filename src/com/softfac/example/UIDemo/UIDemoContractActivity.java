package com.softfac.example.UIDemo;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.EditText;
import android.view.LayoutInflater;
import android.widget.Toast;

/**
 * Created with IntelliJ IDEA.
 * User: byungho
 * Date: 13. 2. 1
 * Time: 오후 2:33
 * To change this template use File | Settings | File Templates.
 */
public class UIDemoContractActivity extends Activity {

    EditText edtName;
    EditText edtPhoneNumber;
    EditText edtDesc;

    Button btnOk;
    Button btnCancel;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.contact);

        edtName = (EditText) findViewById(R.id.edtName);
        edtPhoneNumber = (EditText) findViewById(R.id.edtPhoneNumber);
        edtDesc = (EditText) findViewById(R.id.edtDesc);

        Intent itCaller = getIntent();

        if (null == itCaller) {
            Log.e("UIDemo", "ContactActivity.onCreate() -> Intent is null");
        } else {
            edtName.setText(itCaller.getStringExtra("name"));
            edtPhoneNumber.setText(itCaller.getStringExtra("phonenumber"));
            edtDesc.setText(itCaller.getStringExtra("desc"));
        }

        btnOk = (Button) findViewById(R.id.btnOk);
        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.e("UIDemo", "ContactActivity -> btnOk.onClick()");

                //To change body of implemented methods use File | Settings | File Templates.
                //Intent itReturn = getIntent();
                Intent itReturn = new Intent();
                //Intent itReturn = new Intent(UIDemoContractActivity.this, UIDemoMyActivity.class);
                itReturn.putExtra("name", edtName.getText().toString());
                itReturn.putExtra("phonenumber", edtPhoneNumber.getText().toString());
                itReturn.putExtra("desc", edtDesc.getText().toString());

                Log.e("UIDemo", "ContactActivity -> Ok Clicked -> Save: " + itReturn.getStringExtra("name"));

                setResult(RESULT_OK, itReturn);
                finish();
            }
        });

        Log.e("UIDemo", "ContactActivity.onCreate() -> 7");

        btnCancel = (Button) findViewById(R.id.btnCancel);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.e("UIDemo", "ContactActivity -> btnCancel.onClick()");

                //To change body of implemented methods use File | Settings | File Templates.
                Log.e("UIDemo", "ContactActivity -> Cancel Clicked");
                setResult(RESULT_CANCELED);
                finish();
            }
        });
    }
}

