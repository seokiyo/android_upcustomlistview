package com.softfac.example.UIDemo;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.*;
import android.widget.*;
import android.util.Log;
import android.content.Context;

import java.util.ArrayList;

public class UIDemoMyActivity extends Activity {

    final static int CONTACT_EDIT = 1;
    final static int CONTACT_ADD = 2;

    private static final int ENTRY_MODIFY = Menu.FIRST;
    private static final int ENTRY_REMOVE = Menu.FIRST + 1;

    ListView lvItems;

    // Basic
    ArrayList<String> mListItems;
    ArrayAdapter<String> adapter;

    // Custom
    ArrayList<DemoContact> mContactItems;
    DemoContactAdapter mContactAdapter;

    int iEditPosition;


    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        iEditPosition = -1;

        Log.e("UIDemo", "OnCreate");

        lvItems = (android.widget.ListView) findViewById(R.id.listView);

/*
        // 1. Make list items
        mListItems = new ArrayList<String>();
        mListItems.add("Korean!!");
        mListItems.add("English");
        mListItems.add("WOW");
        mListItems.add("ListView");
        mListItems.add("Vertical Scrollbar");

        // 2. Make adapter
        adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, mListItems);

        // 3. Link data and listview using adapter
        lvItems.setAdapter(adapter);

        Log.e("UIDemo", "OnCreate - After Setup ListView");

        // 4. Setup click listener
        lvItems.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Log.e("UIDemo", "ListView.OnItemClick() - Event: " + mListItems.get(i));
                //To change body of implemented methods use File | Settings | File Templates.
                //Toast.makeText(UIDemoMyActivity.this, mListItems.get(i), Toast.LENGTH_SHORT).show();

                iEditPosition = i;

                Intent itContact = new Intent(UIDemoMyActivity.this, UIDemoContractActivity.class);
                itContact.putExtra("name", mListItems.get(iEditPosition));

                //startActivity(itContact);
                startActivityForResult(itContact, CONTACT_EDIT);

                Log.e("UIDemo", "ListView.OnItemClick() - Event: " + mListItems.get(i) + " -> Started Contact Activity");
            }
        });
*/

        mContactItems = new ArrayList<DemoContact>();
        mContactAdapter = new DemoContactAdapter(this, mContactItems);

        lvItems.setAdapter(mContactAdapter);

        mContactAdapter.add(new DemoContact(getApplicationContext(), "Byunho Jeon", "010-5655-0011", "bastad@softfac.co.kr"));
        mContactAdapter.add(new DemoContact(getApplicationContext(), "Juhee Won", "010-xxxx-2222", "wonjuhee@softfac.co.kr"));
        mContactAdapter.add(new DemoContact(getApplicationContext(), "Seokiyo", "010-1111-222", "seokiyo@softfac.co.kr"));
        mContactAdapter.add(new DemoContact(getApplicationContext(), "gu.youn", "010-333-4544", "gu.youn@softfac.co.kr"));


        lvItems.setCacheColorHint(0);
        lvItems.setOnCreateContextMenuListener(new View.OnCreateContextMenuListener() {
            public void onCreateContextMenu(ContextMenu menu, View v,
                                            ContextMenu.ContextMenuInfo menuInfo) {
                menu.add(0, ENTRY_MODIFY, 1, "Modify");
                menu.add(0, ENTRY_REMOVE, 2, "Remove");

                //Log.e("UIDemo", "ListView.onCreateContextMenu()");
                if(v == (android.widget.ListView) findViewById(R.id.listView))
                {
                    iEditPosition = ((AdapterView.AdapterContextMenuInfo)menuInfo).position;
                    Log.e("UIDemo", "ListView.OnItemClick() -> " + String.format("Position=%d", iEditPosition));
                }
        }


        });
        //Dialog d = new Dialog(this);
        //d.setContentView(lv);
        //d.show();
        //setContentView(lvItems);



        lvItems.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                //To change body of implemented methods use File | Settings | File Templates.
                Log.e("UIDemo", "ListView.OnItemClick() -> " + String.format("Position=%d", i));

                DemoContact contact = mContactItems.get(i);
                if (null == contact) Log.e("UIDemo", "ListView.OnItemClick() -> contact is null.");

                Log.e("UIDemo", "ListView.OnItemClick() - Event");

                //Toast.makeText(UIDemoMyActivity.this, mListItems.get(i), Toast.LENGTH_SHORT).show();
                if (null != contact) {
                    iEditPosition = i;

                    Intent itContact = new Intent(UIDemoMyActivity.this, UIDemoContractActivity.class);
                    itContact.putExtra("name", contact.getName());
                    itContact.putExtra("phonenumber", contact.getPhoneNumber());
                    itContact.putExtra("desc", contact.getDescription());

                    //startActivity(itContact);
                    startActivityForResult(itContact, CONTACT_EDIT);

                    Log.e("UIDemo", "ListView.OnItemClick() - Event -> Started Contact Activity");
                }
            }
        });


        Button button = (Button)findViewById(R.id.button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //To change body of implemented methods use File | Settings | File Templates.
                Log.e("UIDemo", "Button.OnClick");

                Intent itContact = new Intent(UIDemoMyActivity.this, UIDemoContractActivity.class);
                startActivityForResult(itContact, CONTACT_ADD);
            }
        });
    }


    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info =
                (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        Log.e("UIDemo", "Selected item " + info.position);

        switch(item.getItemId()) {
            case ENTRY_MODIFY:
                Log.e("UIDemo", "Modify was clicked");
                DemoContact contact = mContactItems.get(iEditPosition);
                Intent itContact = new Intent(UIDemoMyActivity.this, UIDemoContractActivity.class);
                itContact.putExtra("name", contact.getName());
                itContact.putExtra("phonenumber", contact.getPhoneNumber());
                itContact.putExtra("desc", contact.getDescription());

                //startActivity(itContact);
                startActivityForResult(itContact, CONTACT_EDIT);

                break;
            case ENTRY_REMOVE:
                Log.e("UIDemo", "Remove was clicked");
                Log.e("UIDemo", "ListView.OnItemClick() -> " + String.format("Position=%d", iEditPosition));

                AlertDialog.Builder alert_confirm = new AlertDialog.Builder(UIDemoMyActivity.this);
                alert_confirm.setMessage("Are you sure you want to delete this?").setCancelable(false).setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                mContactItems.remove(iEditPosition);
                                lvItems.invalidateViews();

                                Toast toast = Toast.makeText(UIDemoMyActivity.this, "Have been deleted",Toast.LENGTH_SHORT);
                                toast.show();

                                // 'YES'
                            }
                        }).setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                //Log.e("UIDemo", "dialog_result " + check);
                                // 'No'
                                return;
                            }
                        });
                AlertDialog alert = alert_confirm.create();
                alert.show();

                break;
            default:
                return super.onContextItemSelected(item);
        }
        return true;
    }



    @Override
    public void onDestroy() {
        super.onDestroy();

        //if (mListItems) {
            //mListItems
        //    mListItems = nil;
        //}
    }

//    @Override
//    protected void onListItemClick(android.widget.ListView l, android.view.View v, int position, long id) {
//        Toast.makeText(this, mListItems.get(position), Toast.LENGTH_SHORT).show();
//    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.e("UIDemo", "onActivityResult() - " + String.format("req=%d, res=%d", requestCode, resultCode));

        switch (requestCode) {
            case CONTACT_EDIT:
                if (RESULT_OK == resultCode) {
                    Log.e("UIDemo", "onActivityResult() -> OK");
                    //Log.e("UIDemo", String.format("name=%s", data.getStringExtra("name")));

                    if (null == data) Log.e("UIDemo", "onAcitivityResult() -> OK. but Intent is null.");

                    if ((null != data) && (-1 < iEditPosition)) {
                        //mListItems.set(iEditPosition, data.getStringExtra("name"));

                        DemoContact contact = mContactItems.get(iEditPosition);
                        if (null != contact) {
                            contact.setName(data.getStringExtra("name"));
                            contact.setPhonenumber(data.getStringExtra("phonenumber"));
                            contact.setDescription(data.getStringExtra("desc"));

                            Log.e("UIDemo", "Contact data updated. " + String.format("name=%s, phonenumber=%s, desc=%s",
                                    contact.getName(), contact.getPhoneNumber(), contact.getDescription()));

                            lvItems.invalidateViews();

                            Toast toast = Toast.makeText(this, "Have been modifyed",Toast.LENGTH_SHORT);
                            toast.show();
                        }

                        // TODO: Invalidate?
                    }

                } else {
                    Log.e("UIDemo", "onActivityResult() -> FAIL to edit name from ContactActivity");
                }
            break;
            case CONTACT_ADD:
                if (RESULT_OK == resultCode) {
                    Log.e("UIDemo", "onActivityResult() -> OK");
                    //Log.e("UIDemo", String.format("name=%s", data.getStringExtra("name")));

                    if (null == data) Log.e("UIDemo", "onAcitivityResult() -> OK. but Intent is null.");

                    if ((null != data)) {
                        //mListItems.set(iEditPosition, data.getStringExtra("name"));
                        mContactAdapter.add(new DemoContact(getApplicationContext(), data.getStringExtra("name"), data.getStringExtra("phonenumber"), data.getStringExtra("desc")));
                        Log.e("UIDemo", "Contact data created.");
                        lvItems.invalidateViews();
                        Toast toast = Toast.makeText(this, "Have been created",Toast.LENGTH_SHORT);
                        toast.show();


                        // TODO: Invalidate?
                    }

                } else {
                    Log.e("UIDemo", "onActivityResult() -> FAIL to edit name from ContactActivity");
                }
            break;
        }

    }


}

