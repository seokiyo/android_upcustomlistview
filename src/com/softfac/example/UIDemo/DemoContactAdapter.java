package com.softfac.example.UIDemo;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: byungho
 * Date: 13. 2. 1
 * Time: 오후 4:38
 * To change this template use File | Settings | File Templates.
 */

public class DemoContactAdapter extends ArrayAdapter<DemoContact> {
    private LayoutInflater mInflater;

    public DemoContactAdapter(Context context, ArrayList<DemoContact> object) {
        super(context, 0, object);

        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View getView(int position, View v, ViewGroup parent) {
        View view = null;
        if (null == v) {
            view = mInflater.inflate(R.layout.listitem, null);
        } else {
            view = v;
        }

        final DemoContact data = this.getItem(position);
        if (null != data) {
            TextView text1 = (TextView) view.findViewById(R.id.textView1);
            TextView text2 = (TextView) view.findViewById(R.id.textView2);
            TextView text3 = (TextView) view.findViewById(R.id.textView3);

            text1.setText(data.getName());
            text2.setText(data.getPhoneNumber());
            text3.setText(data.getDescription());
        }

        return view;
    }
}
